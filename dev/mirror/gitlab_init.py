#!/usr/bin/python

import gitlab
import random
import string
import sys
import time

CONF = "/local_gitlab.py"
KEY_FILE = "/root/.ssh/id_rsa.pub"

HOST = "http://gitlab:80"
USER = "root"
PASS = "5iveL!fe"

MIRROR_USER = "xemirror"
MIRROR_PASS = ''.join(random.SystemRandom().choice(string.uppercase + string.digits) for _ in xrange(16))

GROUP = "xe"
ACCESS_LEVEL = 40

git = gitlab.Gitlab(HOST)
while True:
	try:
		print "Logging into", HOST
		if git.login(USER, PASS):
			break
	except KeyboardInterrupt:
		raise 
	except:
		print "Error logging in. Gitlab might not be up yet."
		print sys.exc_info()[0]
	
	print "Waiting a minute..."
	time.sleep(60)

print "Creating mirror user..."
res = git.createuser(MIRROR_USER, MIRROR_USER, MIRROR_PASS, "nobody@nowhere.com")
if res:
	print "User created."
else:
	print "User already exists."
user_obj = git.getusers(MIRROR_USER)[0]
	
print "Creating XE group..."	
res = git.creategroup("XE Development", GROUP)
if res:
	print "Group created."
else:
	print "Group already exists."
group_id = [ g for g in git.getgroups() if g['path'] == 'xe' ][0]['id']
	
print "Adding mirror user to XE group..."
res = git.addgroupmember(group_id, user_obj['id'], ACCESS_LEVEL)
if res:
	print "User added."
else:
	print "User already added."
	
print "Adding SSH key..."
git.setsudo(MIRROR_USER)
fp = open(KEY_FILE, 'r')
ssh_key = fp.read()
fp.close()
res = git.addsshkey('ellucian_git_mirror', ssh_key)
if res:
	print "SSH key added."
else:
	print "SSH key already exists."

print "Getting private key..."
token = git.currentuser()['private_token']

print "Writing configuration file:", CONF
fp = open(CONF, 'w')
print >>fp, 'HOST="%s"' % HOST
print >>fp, 'TOKEN="%s"' % token
print >>fp, 'GROUP="%s"' % GROUP
fp.close()
