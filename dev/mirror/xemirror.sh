#!/bin/bash

ORIGIN_HOST="git@banner-src.ellucian.com"
ORIGIN_URL="ssh://$ORIGIN_HOST"
MIRROR_DIR="/mirror"
NAP_HOURS=12
SKIP_FILE="/skip_repos"
GITLAB_CONF="/local_gitlab.py"

CHECKFILE="last_fetched"

die () {
	echo "$*"
	exit 1
}

remote_repo_list () {
	ssh $ORIGIN_HOST info |grep '^[[:space:]]*R' |grep -o 'banner/.*' || die
}

ref_names () {
	git for-each-ref --format='%(refname)' || die
}

remote_branch_refs () {
	ref_names | grep -v '/HEAD$' |grep -v '/master$' |grep '^refs/remotes/origin' |sed -e 's|refs/||'
}

rule () {
	echo "-------------------------------------------------------------------------------"
}

mirror () {
	url=$1
	section=`dirname $url`
	section=`basename $section`
	name=`basename $url .git`	

	mkdir -p $section || die
	pushd $section || die
	git clone --bare $url || die
	#cd $name.git || die
	#update_local
	popd || die
}

update_local () {
	#git fetch -p origin || die
	#
	#for ref in `remote_branch_refs`; do
	#	remote_name=`basename $ref`
	#	local_name="ellucian/$remote_name"
	#	git show-ref --heads | grep -q "$local_name\$" || git branch --track $local_name $ref || die
	#done
	git fetch -p --all --tags || die
}

# Add SSH keys to known_hosts
[ -f /root/.ssh/known_hosts ] || ssh-keyscan gitlab banner-src.ellucian.com >>/root/.ssh/known_hosts
grep -q banner-src.ellucian.com root/.ssh/known_hosts && ssh-keyscan banner-src.ellucian.com >>/root/.ssh/known_hosts
grep -q banner-src.ellucian.com root/.ssh/known_hosts || die "failed to get key for banner-src.ellucian.com"

while true; do
	grep -q gitlab root/.ssh/known_hosts && break
	echo "No entry in known_hosts for gitlab. Gitlab may not be up yet. I'll try again in a minute..."
	sleep 60
    ssh-keyscan gitlab >>/root/.ssh/known_hosts
done	


mkdir -p $MIRROR_DIR || die "failed to create mirror directory"
cd $MIRROR_DIR || die "cannot cd to mirror directory"

if [ -f "$GITLAB_CONF" ]; then
	echo "Found GitLab configuration: $GITLAB_CONF"
else
	echo "GitLab configuration file not found: $GITLAB_CONF"
	echo "Initializing GitLab..."
	python /gitlab_init.py || die "error Initializing GitLab"
fi

while true; do
	echo "Updating mirror..."
	for p in `remote_repo_list`; do
		rule
		
		local_repo=`echo "$p" |sed -e 's|^banner/||'`
		local_dir=$local_repo.git
		check_file="$local_dir/last_fetched"
		
		if grep -q "$local_repo" "$SKIP_FILE"; then
			echo "Skipping $local_repo"
			continue
		fi
		
		if [ -d $local_dir ]; then
			if [ -z "$(find $local_dir -name $CHECKFILE -mmin +60 |head -n 1)" ]; then
				echo "$local_repo has been updated in the last hour."
				echo "Will not pull from Ellucian."
			else		
				echo "Updating $local_repo..."
				pushd $local_dir || die
				update_local
				touch $CHECKFILE
				popd || die
			fi
		else
			echo "Cloning $local_repo..."
			remote_url="$ORIGIN_URL/$p.git"
			mirror $remote_url
			touch $local_dir/$CHECKFILE
		fi
		echo
		
		python /update_gitlab_project.py "$local_dir" || die
		
		pushd $local_dir ||die
		echo "Pushing to GitLab..."
		git push --all gitlab ||die
		git push --tags gitlab ||die
		popd
		echo
		
	done
	
	echo "Mirror update complete."
	echo "Napping $NAP_HOURS hours..."
	sleep $(($NAP_HOURS * 60 * 60))
done
