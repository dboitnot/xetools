#!/usr/bin/python

import gitlab
import git
import git.remote
import sys
import os.path

from local_gitlab import *

REMOTE_NAME = 'gitlab'

def get_all_projects(gl):
	i = 1
	while True:
		page = gl.getprojects(page=i)
		if len(page) == 0:
			return
		for p in page:
			yield p
		i = i + 1
		

repo_path = sys.argv[1]
basename = os.path.basename(os.path.splitext(repo_path)[0])
category = os.path.basename(os.path.dirname(repo_path))
#project_name = "%s_%s" % (category, basename)
project_name = basename

repo = git.Repo(repo_path)
l = [x for x in repo.remotes if x.name == REMOTE_NAME]
if len(l) > 0:
	print "Remote already exists:", REMOTE_NAME
else:
	print "Remote not found:", REMOTE_NAME
	print "Configuring project in GitLab..."
	print "Connecting to host:", HOST
	gl = gitlab.Gitlab(HOST, TOKEN)
	
	project = [x for x in get_all_projects(gl) if x['path'] == project_name]
	if len(project) > 0:
		project = project[0]
		print "Project already exists:", project_name
	else:	
		print "Creating project:", project_name
		group_id = [x for x in gl.getgroups() if x['path'] == GROUP][0]['id']
		if not gl.createproject(project_name, namespace_id=group_id):
			raise Exception("Error creating project")
		print "Project created."
		project = [x for x in get_all_projects(gl) if x['path'] == project_name][0]
		
	# If the URL doesn't start with 'git@localhost:' then the user has updated the
	# hostname so keep it. Otherwise, use the link.
	url = project['ssh_url_to_repo'].replace("git@localhost:", "git@gitlab:")
	
	print "Adding remote to repo:", REMOTE_NAME
	git.remote.Remote.create(repo, REMOTE_NAME, url)
